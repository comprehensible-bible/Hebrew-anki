# Hebrew-anki



Utilising Comprehensible Input as an ideal, this Hebrew–English and English–Hebrew flashcard deck is semi-functional monolingually, utilising pictures where possible instead of English words. All prompts are in English, and many pictures are a work-in-progress (WIP) to better middle the semantic range and remove ambiguities. The deck is keyed to Hackett's A Basic Introduction to Biblical Hebrew, with an extensive tagging system (WIP). Additional lexica (dictionaries) have been consulted, especially for post-Hackett vocabulary (see below), these will be noted by tags.

This is the corresponding git project hosted on GitLab for changelog, milestones, issue tracking and collaboration (hopefully) as the AnkiWeb shared deck [here](https://ankiweb.net/shared/info/1662721723).

 The third deck follows a corriculum used by my teacher post finishing Hackett, and aside from some obscure Jonah Vocab, continues learning vocabulary common throughout narrative components of the Tanak / TaNaK / Hebrew Old Testament jewish scriptures.

## Tagging

Anki performs better with fewer decks and is best used by making use of it's Custom Study Session tools for breaking larger collections into sizable chunks, such as Hackett chapters, or cramming for specific purposes. Because of this, tagging is the best way to allow a student who may have discovered this deck in week 2 of semester and want chapters 2–4 for review in one deck and chapter 5 for learning in another. To do so, simply create a custom deck only including notes by particular tags: *Study by card state or tag* > Choose Tags > Require one or more.. > and select "ch03", "ch04". For maximum compatibility with mixed decks (say a collection that includes notes keyed to a different grammar (textbook]), your resulting deck (under Options) might  be "tag:heb-hackett (tag:ch03 OR tag:ch04) "deck:Hebrew::Biblical Hebrew by Jo Ann Hackett::Hebrew A+B".

The tagging system is meant to both hierachical (implemented) and also split for use by advanced users (heb_* ; technologically and as a Hebrew student) and more casual users (WIP). The idea being that simple vocabulary or paradigm cards will have the tag starting with "hebrew_" and any note with a sub _tag, such as the grammar tags heb_morph-token-nif *should* have the parent tags as well, such as heb_morph-token. heb_morph-token-nif has three parts, "heb_" meaning complex or falls more under grammar than vocabulary; "morph-token" – my nickname for "this word is morphologically inflected, i.e. not the lemma (base, lexical form)"; and "nif" meaning that these notes are in the נפעל nifal / niphal stem. For typical nifals "verb-nifal" will be used.

## Terminology ( currently very inconsistent :( ]
My ideal is where possible to use symbols closer to hackett, but for compatibility, file names and tags have my own system of transliteration:
Shin is "sh" BeGheDhKeFeTh letters will have an h following whenever the "soft" pronunciation is expected (no daghesh), excluding Pey as below:
Sh'va, ּבְ, (dummy beth) is extremely inconsistent. You will find a mix of apostrophai (') as the vowel, as well as the IPA character (upside down e) and ocassionally a period (.) – the last which is confusing as it is also an , א, Alef.
In general I prefer "f" for a , פ , Pey without a daghesh. When shortening stem names I tend to use three letters, however where the final vowel is hard to remember I will not shorten, and thus you should see:
nifal (sometimes *nif*) hifil (sometimes *hif), hofal (hopefully never *hof* – since the a vowel highlight the crucial difference between it's active counterpart, the hifil).
QPass refers to the Qal passive.

Abbreviations:
```
hitpolel - hlel
hitpolal - hlal
I-Gutt - first guttaral
III-Weak - third weak verb
ptc - participle
pass - passive
pi - piel
ni - nifal
etym - etymology
pic - picture
heb-meek-osborne - A Book by Book Guide to Biblical Hebrew, by Osborne & Meek
heb-HITS - a tool on Accordance software
heb-Pealim - pealim.com
heb-ph - Picture Hebrew, https://www.picturehebrew.com/
heb-stellenbosch – https://ankiweb.net/shared/info/1129659006
heb-vanpelt-pratico - Old Testeament Hebrew Flashcards, by Van Pelt and Pratico (Zondervan)
```


## Getting started

WIP




## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
1.0 Release is when Hebrew A + B is functional and feature complete with images and audio for every vocabulary item.

1.1 I would like 1.1 to focus on helping a student under a stringent teacher to excel in a Hebrew class by having cards that help memorise irregular forms - plurals, with suffixes, roots for lemmas that lose consonants etc. Also common irregular verbs such as ראה (ra.ah – he saw)

1.2 I would like to have all words that occur 50x or more in the Hebrew Bible. I personally haven't achieved this and I think the lack of digital access has played a role in me not being inspired to continue. I will rely heavily on Van Pelt & Pratico and my own personal study for this.

## Contributing
I am open to contributions. Please keep this project as Free as possible and use CreativeCommons wherever possible. My own systems are fragmented enough, so please consider creating a standard for me to adopt to before just making your own note type that's incompatible with mine.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started.


## Authors and acknowledgment
Thanks to GlossaHouse and Aleph with Beth https://freehebrew.hismagnificence.com/resources/
Also many thanks to GCTS-Hebrew from quizlet: https://quizlet.com/GCTS-Hebrew for the bulk of the work.

## License
The database is licensed under Creative Commons Zero.
The assets are marked individually by tagging and custom fields, but for the most part are creative commons. Where there is no license, all rights belong to the original intellectual property holder.

If you would like to make a takedown request, please contact me at lekhetshalom (æt) yvah (d0t) one
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
